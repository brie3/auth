package main

import (
	"auth/proto/auth"
	"auth/service"
	"log"
	"net"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	_ "github.com/golang-migrate/migrate/source/file"
	"github.com/prometheus/client_golang/prometheus/promhttp"

	"google.golang.org/grpc"
)

func main() {
	conf, err := service.SetupVars()
	if err != nil {
		log.Fatal(err)
	}

	as, err := service.New(conf)
	if err != nil {
		log.Fatal(err)
	}

	defer func() {
		log.Fatal(as.Stop())
	}()

	// prometheus
	go func() {
		http.Handle("/metrics", promhttp.Handler())
		log.Fatal(http.ListenAndServe(conf.PrometheusAddr, nil))
	}()

	// health
	go func() {
		http.HandleFunc("/__health__", healthCheck)
		log.Fatal(http.ListenAndServe(conf.HealthAddr, nil))
	}()

	srv := grpc.NewServer(grpc.UnaryInterceptor(service.RecoverInterceptor))
	auth.RegisterAuthServiceServer(srv, as)

	listener, err := net.Listen("tcp", conf.ServiceAddr)
	if err != nil {
		log.Fatalf("auth service failed to listen: %v", err)
	}

	log.Printf("starting auth service on %s", conf.ServiceAddr)
	log.Fatal(srv.Serve(listener))
}

func healthCheck(w http.ResponseWriter, _ *http.Request) {
	w.WriteHeader(http.StatusOK)
}
