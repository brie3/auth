module auth

go 1.15

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-sql-driver/mysql v1.5.0
	github.com/golang-migrate/migrate v3.5.4+incompatible
	github.com/golang/protobuf v1.4.2
	github.com/prometheus/client_golang v1.7.1
	google.golang.org/grpc v1.33.0
	google.golang.org/protobuf v1.25.0
)
