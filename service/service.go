// Package service implements authorization interactions.
package service

import (
	"context"
	"crypto/hmac"
	"crypto/rsa"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"io/ioutil"
	"log"
	"os"
	"runtime/debug"
	"time"

	"auth/proto/user"
	pbUser "auth/proto/user"

	"github.com/dgrijalva/jwt-go"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

const (
	alg                    = "RS256"
	unauthorized           = "unauthorized"
	internalFormat         = "internal error"
	errorFormat            = "[%s] error handled: %v"
	recoverFormat          = "[%s] recovered from %v, %s"
	createPwdHashErrFormat = "can't create pwd hash: %v"
	createTokenErrFormat   = "can't create token string: %v"
	ttl                    = time.Duration(time.Hour * 72)
	key                    = "x-request-id"
)

type Config struct {
	PrometheusAddr  string
	ServiceAddr     string
	UserServiceAddr string
	AuthKeyPath     string
	HealthAddr      string
}

// Principal stands for custom type for use within auth token.
type Principal struct {
	ID    int32
	Role  int32 // user role (1 - client; 2 - admin)
	Email string
}

// Claim stands for auth service claim.
type Claim struct {
	*jwt.StandardClaims
	Principal
}

// AuthService handles auth.
type AuthService struct {
	userCli   user.UserServiceClient
	userConn  *grpc.ClientConn
	signKey   *rsa.PrivateKey // openssl genrsa -out auth_key 2048 | openssl rsa -in auth_key -pubout > aut_key.pub
	signBytes []byte
}

// New brings to live new auth service.
func New(c *Config) (*AuthService, error) {
	key, err := setupKey(c)
	if err != nil {
		return nil, err
	}
	connUser, err := grpc.Dial(c.UserServiceAddr, grpc.WithInsecure(), grpc.WithBlock())
	if err != nil {
		return nil, err
	}
	return &AuthService{
		userCli:   pbUser.NewUserServiceClient(connUser),
		userConn:  connUser,
		signKey:   key,
		signBytes: key.N.Bytes(),
	}, nil
}

func (a *AuthService) Stop() error {
	return a.userConn.Close()
}

func SetupVars() (*Config, error) {
	var ok bool
	conf := &Config{}
	conf.PrometheusAddr, ok = os.LookupEnv("PROMETHEUS_ADDR")
	if !ok {
		return nil, errors.New("PROMETHEUS_ADDR env is not set")
	}
	conf.ServiceAddr, ok = os.LookupEnv("SERVICE_ADDR")
	if !ok {
		return nil, errors.New("SERVICE_ADDR env is not set")
	}
	conf.UserServiceAddr, ok = os.LookupEnv("USER_SERVICE_ADDR")
	if !ok {
		return nil, errors.New("USER_SERVICE_ADDR env is not set")
	}
	conf.AuthKeyPath, ok = os.LookupEnv("AUTH_KEY_PATH")
	if !ok {
		return nil, errors.New("AUTH_KEY_PATH env is not set")
	}
	conf.HealthAddr, ok = os.LookupEnv("HEALTH_ADDR")
	if !ok {
		return nil, errors.New("HEALTH_ADDR env is not set")
	}
	return conf, nil
}

func setupKey(c *Config) (*rsa.PrivateKey, error) {
	signBytes, err := ioutil.ReadFile(c.AuthKeyPath)
	if err != nil {
		return nil, err
	}
	out, err := jwt.ParseRSAPrivateKeyFromPEM(signBytes)
	if err != nil {
		return nil, err
	}
	return out, nil
}

func (a *AuthService) createToken(id, role int32, email string) (string, error) {
	t := jwt.New(jwt.GetSigningMethod(alg))
	t.Claims = &Claim{
		&jwt.StandardClaims{
			ExpiresAt: time.Now().Add(ttl).UTC().Unix(),
		},
		Principal{ID: id, Email: email, Role: role},
	}
	return t.SignedString(a.signKey)
}

func (a *AuthService) createPwdHash(pwd string) (string, error) {
	mac := hmac.New(sha256.New, a.signBytes)
	if _, err := mac.Write([]byte(pwd)); err != nil {
		return "", err
	}
	return hex.EncodeToString(mac.Sum(nil)), nil
}

func wrapRecover(ctx context.Context, r interface{}) error {
	log.Printf(recoverFormat, getRid(ctx), r, debug.Stack())
	return status.Errorf(codes.Internal, internalFormat)
}

func wrapError(ctx context.Context, err error) error {
	log.Printf(errorFormat, getRid(ctx), err)
	return err
}

func getRid(ctx context.Context) string {
	if md, ok := metadata.FromIncomingContext(ctx); ok {
		if arr := md.Get(key); len(arr) > 0 {
			return arr[0]
		}
	}
	return ""
}

// RecoverInterceptor handles recovery from unexpected service behavior.
func RecoverInterceptor(
	ctx context.Context,
	req interface{},
	info *grpc.UnaryServerInfo,
	handler grpc.UnaryHandler,
) (resp interface{}, err error) {
	defer func() {
		if r := recover(); r != nil {
			err = wrapRecover(ctx, r)
		}
	}()
	return handler(metadata.AppendToOutgoingContext(ctx, key, getRid(ctx)), req)
}
