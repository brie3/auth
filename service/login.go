package service

import (
	"auth/proto/auth"
	"auth/proto/user"
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// Login returns jwt.
func (a *AuthService) Login(
	ctx context.Context,
	req *auth.LoginRequest,
) (
	*auth.LoginResponse,
	error,
) {
	u, err := a.userCli.GetUser(ctx, &user.UserRequest{Email: req.GetEmail()})
	if err != nil {
		return nil, wrapError(ctx, err)
	}

	pwdHash, err := a.createPwdHash(req.GetPwd())
	if err != nil {
		return nil, wrapError(ctx, status.Errorf(codes.Internal, createPwdHashErrFormat, err))
	}

	if pwdHash != u.PwdHash {
		return nil, wrapError(ctx, status.Error(codes.PermissionDenied, unauthorized))
	}

	tokenString, err := a.createToken(u.GetId(), int32(u.Role), u.GetEmail())
	if err != nil {
		return nil, wrapError(ctx, status.Errorf(codes.Internal, createTokenErrFormat, err))
	}
	return &auth.LoginResponse{Jwt: tokenString}, nil
}
