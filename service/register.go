package service

import (
	"auth/proto/auth"
	"auth/proto/user"
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// Register registers new user.
func (a *AuthService) Register(
	ctx context.Context,
	req *auth.RegisterRequest,
) (
	*auth.RegisterResponse,
	error,
) {
	pwdHash, err := a.createPwdHash(req.GetPwd())
	if err != nil {
		return nil, wrapError(ctx, status.Errorf(codes.Internal, createPwdHashErrFormat, err))
	}

	u, err := a.userCli.CreateUser(ctx, &user.CreateUserRequest{Email: req.GetEmail(), PwdHash: pwdHash})
	if err != nil {
		return nil, wrapError(ctx, err)
	}

	tokenString, err := a.createToken(u.GetId(), 0, req.GetEmail())
	if err != nil {
		return nil, wrapError(ctx, status.Errorf(codes.Internal, createTokenErrFormat, err))
	}
	return &auth.RegisterResponse{Jwt: tokenString}, nil
}
