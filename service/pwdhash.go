package service

import (
	"auth/proto/auth"
	"context"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// Login returns jwt.
func (a *AuthService) PwdHash(
	ctx context.Context,
	req *auth.PwdRequest,
) (
	*auth.PwdResponse,
	error,
) {
	pwdHash, err := a.createPwdHash(req.GetPwd())
	if err != nil {
		return nil, wrapError(ctx, status.Errorf(codes.Internal, createPwdHashErrFormat, err))
	}
	return &auth.PwdResponse{PwdHash: pwdHash}, nil
}
